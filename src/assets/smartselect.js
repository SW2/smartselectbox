$(function () {

    var $content = $('body');

	/**
	 * Init smart select box.
	 * @param $ctx
	 */
	var initSmartSelectBox = function ($ctx) {
		$ctx.find('input.smart-select').each(function (i, el) {
			var $container = $(el).closest('.input-group');
			var $input = $container.find('input[type=hidden]');
			var $successIcon = $container.find('.smart-ok');
			var $removeButton = $container.find('.smart-remove');
			$(el).autocomplete({
				autoFocus: true,
				source: function (request, response) {
					$.getJSON($(el).data('api-url'), {q: request.term}, function (data) {
						console.log(data);
						if (data.status == 'ok') {
							response(data.items);
						}
					});
				},
				select: function (event, ui) {
					$input.val(ui.item.id);
					$successIcon.show();
					$(el).prop('disabled', true);
					$removeButton.show();
				}
			});
			$removeButton.click(function (ev) {
				ev.preventDefault();
				$input.val(null);
				$successIcon.hide();
				$(el).prop('disabled', false).val(null);
				$removeButton.hide();
			});
		});
	};
	$.nette.ext('snippets').after(initSmartSelectBox);
	initSmartSelectBox($content);

});