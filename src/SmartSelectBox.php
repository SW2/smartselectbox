<?php

namespace Sw2;

use Nette\Application\UI\Form;
use Nette\Forms\Controls\BaseControl;
use Nette\Utils\Html;

/**
 * Class AutoCompleteSelectBox
 *
 * @package App\Controls\Forms\Controls
 * @author Lupo112 <lupo112@gmail.com>
 */
class SmartSelectBox extends BaseControl
{
	/** @var string */
	protected $apiUrl;

	/** @var string */
	protected $icon;

	/** @var string|NULL */
	protected $prompt;

	/** @var  bool */
	private $removeEl = TRUE;

	/**
	 * @param string $apiUrl
	 * @param string|NULL $caption
	 * @param string $icon
	 */
	public function __construct($apiUrl, $caption = NULL, $icon = 'puzzle-piece')
	{
		parent::__construct($caption);
		$this->control->type = 'hidden';
		$this->apiUrl = $apiUrl;
		$this->icon = $icon;

		$this->addCondition(Form::FILLED)->addRule(Form::INTEGER);
	}

	/**
	 * @param string $prompt
	 *
	 * @return $this
	 */
	public function setPrompt($prompt)
	{
		$this->prompt = $prompt;

		return $this;
	}

	/**
	 * @param bool $set
	 *
	 * @return $this
	 */
	public function setElAddonRemove($set = TRUE)
	{
		$this->removeEl = $set;

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getControl()
	{
		$hidden = parent::getControl();
		$hidden->addAttributes([
			'id' => NULL,
			'value' => !empty($this->value['id']) ? $this->value['id'] : $this->value,
		]);
		$el = Html::el('div', ['class' => 'input-group']);
		$el->addHtml($this->getElAddonIcon());
		$el->addHtml(Html::el('input', [
			'type' => 'text',
			'id' => $this->htmlId,
			'class' => 'form-control smart-select',
			'data-api-url' => $this->apiUrl,
			'value' => !empty($this->value['name']) ? $this->value['name'] : NULL,
			'disabled' => !empty($this->value),
			'placeholder' => $this->translate($this->prompt),
		]));
		$el->addHtml($this->getElAddonRemove()->addHtml($hidden));

		return $el;
	}

	/**
	 * @return Html
	 */
	private function getElAddonIcon()
	{
		$el = Html::el('span', ['class' => 'input-group-addon']);
		$el->addHtml(Html::el('i', ['class' => "fa fa-{$this->icon}"]));
		$el->addHtml(Html::el('i', ['class' => 'fa fa-check smart-ok', 'style' => empty($this->value) ? 'display:none' : NULL]));

		return $el;
	}

	/**
	 * @return Html
	 */
	private function getElAddonRemove()
	{
		$el = Html::el('a', [
			'class' => 'input-group-addon smart-remove',
			'href' => '#',
			'style' => empty($this->value) || !$this->removeEl ? 'display:none' : NULL,
		]);
		$el->addHtml(Html::el('i', ['class' => 'fa fa-times']));

		return $el;
	}

}
